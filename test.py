import json
import nltk

# Open the JSON file and load the data
with open('res/filipino_sentences.json', 'r') as file:
    sentences = json.load(file)

# Load the JSON data from the pos_to_fil.json file into a Python dictionary
with open('res/pos_to_fil.json', 'r') as file:
    pos_to_fil = json.load(file)


# Define a function to tag each word in Filipino using the loaded dictionary
def tag_filipino(word):
    word = word.lower()  # convert word to lowercase
    for pos, words in pos_to_fil.items():
        if word in words:
            return pos
    return "UNK"  # return "UNK" if the word is not in any list


# Now you can work with the list of sentences
for sentence in sentences:
    tokens = nltk.word_tokenize(sentence)
    tagged_tokens = [(word, tag_filipino(word)) for word in tokens]
    # print(sentence)
    print(tagged_tokens)