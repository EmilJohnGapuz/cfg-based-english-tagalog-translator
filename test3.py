# import json
# import nltk
#
# # Load the sentences from the JSON file
# with open('res/filipino_sentences.json', 'r') as file:
#     sentences = json.load(file)
#
# # Initialize a set to store the words
# words = set()
#
# # Tokenize each sentence into words and add them to the set
# for sentence in sentences:
#     tokens = nltk.word_tokenize(sentence)
#     words.update(tokens)
#
# # Convert the set to a list
# words = list(words)
#
# # Write the list to a new JSON file
# with open('res/filipino_words.json', 'w') as file:
#     json.dump(words, file)


# import json
#
# # Load the dictionary from the JSON file
# with open('res/dictionary.json', 'r') as file:
#     dictionary = json.load(file)
#
# # Load the words from the JSON file
# with open('res/filipino_words.json', 'r') as file:
#     words = json.load(file)
#
# # Initialize a list to store the translated words
# translated_words = []
#
# # Translate each word using the dictionary
# for word in words:
#     if word in dictionary:
#         translated_words.append(dictionary[word])
#     else:
#         translated_words.append(word)
#
# # Write the translated words to a new JSON file
# with open('res/translated_words.json', 'w') as file:
#     json.dump(translated_words, file)
