import json

import nltk

# Load the predefined Filipino POS tags
with open('res/pos_to_fil.json', 'r') as file:
    pos_to_fil = json.load(file)

# Load the collected Filipino sentences
with open('res/filipino_sentences.json', 'r') as file:
    sentences = json.load(file)


# Tag each word in Filipino using the loaded dictionary
def tag_filipino(word, previous_word):
    word = word.lower()  # convert word to lowercase
    for pos, words in pos_to_fil.items():
        if word in words:
            # If the word can be both a preposition and a conjunction,
            # consider the previous word to determine the correct tag
            if word in pos_to_fil["PREP"] and word in pos_to_fil["CONJ"]:
                if previous_word in pos_to_fil["NOUN"]:
                    return "PREP"
                else:
                    return "CONJ"
            else:
                return pos
    return "UNK"  # return "UNK" if the word is not in any list


# Now you can work with the list of sentences
for sentence in sentences:
    tokens = nltk.word_tokenize(sentence)
    tagged_tokens = [(word, tag_filipino(word, tokens[i-1] if i > 0 else '')) for i, word in enumerate(tokens)]
    # print(sentence)
    # print(tagged_tokens)
    print([tag for word, tag in tagged_tokens])
