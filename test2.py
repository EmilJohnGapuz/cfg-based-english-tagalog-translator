from nltk.parse.generate import generate
from nltk import CFG

# Define your CFG
cfg_string = """
    S -> 'The' NOUN VERB ADV
    NOUN -> 'dog' | 'cat'
    VERB -> 'runs' | 'jumps'
    ADV -> 'fast' | 'high'
"""

# Parse the CFG
cfg = CFG.fromstring(cfg_string)

# Generate sentences
for sentence in generate(cfg, n=10):  # Generate 10 sentences
    print(' '.join(sentence))