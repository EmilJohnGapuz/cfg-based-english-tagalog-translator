from googletrans import Translator
import json

# Load the words from the JSON file
with open('res/filipino_words.json', 'r') as file:
    words = json.load(file)

# Initialize the translator
translator = Translator()

# Initialize a dictionary to store the Filipino words and their English translations
translation_dict = {}

# Translate each word using the translator
for word in words:
    translation = translator.translate(word, src='tl', dest='en')
    translation_dict[word] = translation.text

# Write the dictionary to a new JSON file
with open('res/dictionary.json', 'w') as file:
    json.dump(translation_dict, file)